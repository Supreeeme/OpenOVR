FROM ubuntu:latest
RUN dpkg --add-architecture i386 && \
	apt-get update && \
	apt-get install -y g++ g++-multilib cmake libgl-dev libvulkan-dev libgl-dev:i386 libvulkan-dev:i386
